# Pizza Server (for a lack of a better name...)

Acts as a back-end service that will handle pizza orders, received from Pizza Client.

## Pre-requisite

You will need the following 
* Maven 3.X
* Git
* JDK 1.8

## Installation

You can build your project by running

```cmd
mvn clean package
```

## Usage

* Open cmd and navigate to /target/.  This is where the executable jar lives.
* To execute, run

```cmd
java -jar ./pizzaserver-0.0.1-SNAPSHOT.jar
```