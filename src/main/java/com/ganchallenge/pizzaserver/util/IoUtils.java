package com.ganchallenge.pizzaserver.util;

import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class IoUtils {

  private static Logger log = LoggerFactory.getLogger(IoUtils.class);

  public InputStream getInputStream(String fileLoc) {
    InputStream inputStream = null;
    try {
      log.info("fileLoc: " + fileLoc);
      inputStream = new ClassPathResource(fileLoc).getInputStream();
    } catch (Exception e) {
      log.error("Unable to convert URI to Path, {}", fileLoc, e);
      throw new RuntimeException(e);
    }
    return inputStream;
  }
}
