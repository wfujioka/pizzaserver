package com.ganchallenge.pizzaserver.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

@Component
public class CsvUtil {

  private static Logger log = LoggerFactory.getLogger(CsvUtil.class);

  private IoUtils ioUtils;

  public CsvUtil(IoUtils ioUtils) {
    this.ioUtils = ioUtils;
  }

  public List<String[]> csvToList(String fileLoc) {
    InputStream inputStream = ioUtils.getInputStream(fileLoc);
    List<String[]> lines = new ArrayList<>();
    try (InputStreamReader reader = new InputStreamReader(inputStream); 
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build()) {
      lines = csvReader.readAll();
    } catch (Exception e) {
      log.error("Unable to read csv file, {}", fileLoc, e);
      throw new RuntimeException(e);
    }

    return lines;
  }


}
