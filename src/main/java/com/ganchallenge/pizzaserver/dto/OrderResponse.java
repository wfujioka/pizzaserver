package com.ganchallenge.pizzaserver.dto;

public class OrderResponse {

  private Integer bakeTime;
  private String messageId;

  public Integer getBakeTime() {
    return bakeTime;
  }

  public void setBakeTime(Integer bakeTime) {
    this.bakeTime = bakeTime;
  }

  public String getMessageId() {
    return messageId;
  }

  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }


}
