package com.ganchallenge.pizzaserver.dto;

import java.io.Serializable;
import java.util.Map;

public class OrderCriteria implements Serializable {

  private static final long serialVersionUID = 1L;

  private Map<String, Integer> ingredients;
  private String url;

  public Map<String, Integer> getIngredients() {
    return ingredients;
  }

  public void setIngredients(Map<String, Integer> ingredients) {
    this.ingredients = ingredients;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
