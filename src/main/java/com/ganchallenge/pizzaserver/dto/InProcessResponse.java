package com.ganchallenge.pizzaserver.dto;

public class InProcessResponse {

  private String status;

  public InProcessResponse(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
