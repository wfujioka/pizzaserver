package com.ganchallenge.pizzaserver.controllers;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import com.ganchallenge.pizzaserver.dto.InProcessResponse;
import com.ganchallenge.pizzaserver.dto.OrderCriteria;
import com.ganchallenge.pizzaserver.dto.OrderResponse;
import com.ganchallenge.pizzaserver.services.OrderQueueService;
import com.ganchallenge.pizzaserver.services.OrderService;

@RequestMapping("/orders")
@RestController
public class OrderController {

  private OrderService orderService;
  private OrderQueueService orderQueueService;

  public OrderController(OrderService orderService, OrderQueueService orderQueueService) {
    this.orderService = orderService;
    this.orderQueueService = orderQueueService;
  }

  private ExecutorService threads = Executors.newFixedThreadPool(1000);

  @PostMapping("/submit")
  public OrderResponse submitOrder(@RequestBody OrderCriteria crit) {
    return orderService.prepareAndSendOrderForBaking(crit);
  }

  @GetMapping("/long-poll-until-not-in-queue/{messageId}")
  public DeferredResult<InProcessResponse> isOrderInQueue(@PathVariable String messageId) {
    DeferredResult<InProcessResponse> output = new DeferredResult<>();
    threads.execute(() -> {
      try {
        if (orderQueueService.isOrderInQueue(messageId)) {
          for (int i = 0; i < 15 || orderQueueService.isOrderInQueue(messageId); i++) {
            Thread.sleep(500);
          }
        }

        InProcessResponse outputBody = new InProcessResponse("NOT_IN_QUEUE");
        if (orderQueueService.isOrderInQueue(messageId)) {
          outputBody.setStatus("IN_QUEUE");
        }
        output.setResult(outputBody);
      } catch (Exception e) {
        // add exception here
      }
    });
    return output;
  }

  @GetMapping("/view-queue")
  public List<String> queryQueue() {
    return orderQueueService.getOrdersInQueue();
  }
}
