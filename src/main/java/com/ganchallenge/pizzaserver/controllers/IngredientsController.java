package com.ganchallenge.pizzaserver.controllers;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ganchallenge.pizzaserver.services.IngredientsService;

@RequestMapping("/ingredients")
@RestController
public class IngredientsController {

  private IngredientsService ingredientsService;

  public IngredientsController(IngredientsService ingredientsService) {
    this.ingredientsService = ingredientsService;
  }

  @GetMapping("/available")
  public Map<String, Integer> availableIngredients() {
    return ingredientsService.getAvailableIngredients();
  }

  @PostMapping("/verify")
  public Map<String, Boolean> verifyIngredients(@RequestBody Map<String, Integer> crit) {
    Map<String, Boolean> result = new HashMap<>();
    result.put("isValid", ingredientsService.verifyIngredients(crit));
    return result;
  }

}
