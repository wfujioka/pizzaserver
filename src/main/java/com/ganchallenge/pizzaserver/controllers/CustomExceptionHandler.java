package com.ganchallenge.pizzaserver.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

  protected ResponseEntity<Object> handleRuntimeException(RuntimeException ex, WebRequest request) {
    String body = ex.getMessage();
    return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }
}
