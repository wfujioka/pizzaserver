package com.ganchallenge.pizzaserver.services;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.ganchallenge.pizzaserver.dto.OrderCriteria;

@Service
public class OvenListener implements MessageListener {

  private static Logger log = LoggerFactory.getLogger(OvenListener.class);

  private PizzaClientClient pizzaClientClient;
  @Value("${pizza-server.baking-time}")
  private int bakingTimeInSec;


  public OvenListener(PizzaClientClient pizzaClientClient) {
    this.pizzaClientClient = pizzaClientClient;
  }

  @Override
  public void onMessage(Message message) {
    try {
      log.info("Order {} has started baking...", message.getJMSMessageID());
      Thread.sleep(getBakingTimeInMillisec());
      log.info("Order {} is done", message.getJMSMessageID());
      sendOrderToPizzaClient(message);
    } catch (InterruptedException e) {
      log.warn("thread was interrupted while sleeping", e);
    } catch (JMSException e) {
      log.error("Unable to retrieve the JMSMessageID", e);
    }
  }

  private void sendOrderToPizzaClient(Message message) throws JMSException {
    ObjectMessage objMsg = (ObjectMessage) message;
    OrderCriteria orderCrit = (OrderCriteria) objMsg.getObject();
    if (orderCrit != null) {
      String url = orderCrit.getUrl();
      if (StringUtils.hasText(url)) {
        try {
          pizzaClientClient.sendMessageIdToPizzaClient(message, orderCrit);
        } catch (Exception e) {
          log.error("Unable to send confirmation to pizza client", e);
        }
      } else {
        log.warn("Pizza client url has not been received.  NOT sending acknowledgement over to pizza client");
      }
    }
  }

  private Integer getBakingTimeInMillisec() {
    return bakingTimeInSec * 1000;
  }
}
