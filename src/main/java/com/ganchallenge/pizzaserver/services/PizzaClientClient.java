package com.ganchallenge.pizzaserver.services;

import java.util.HashMap;
import java.util.Map;
import javax.jms.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.ganchallenge.pizzaserver.dto.OrderCriteria;

@Component
public class PizzaClientClient {

  private static final Logger log = LoggerFactory.getLogger(PizzaClientClient.class);

  private RestTemplate restTemplate;

  public PizzaClientClient(RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.build();
  }

  public void sendMessageIdToPizzaClient(Message message, OrderCriteria orderCrit) throws Exception {
    String url = createUrl(message, orderCrit);
    Map<String, String> body = new HashMap<>();
    body.put("messageId", message.getJMSMessageID());
    ResponseEntity<String> result = restTemplate.postForEntity(url, body, String.class);
    if (!HttpStatus.OK.equals(result.getStatusCode())) {
      log.error("Not able to send completed order to Pizza Client: {}", result.getStatusCode());
    }
  }

  private String createUrl(Message message, OrderCriteria orderCriteria) throws Exception {
    return orderCriteria.getUrl() + message.getJMSMessageID();
  }
}
