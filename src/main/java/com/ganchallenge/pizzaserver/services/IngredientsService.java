package com.ganchallenge.pizzaserver.services;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import com.ganchallenge.pizzaserver.repositories.IngredientsRepository;

@Service
public class IngredientsService {

  private IngredientsRepository ingredientsRepository;

  public IngredientsService(IngredientsRepository ingredientRepository) {
    this.ingredientsRepository = ingredientRepository;
  }

  public Map<String, Integer> getAvailableIngredients() {
    return ingredientsRepository.getIngredientsWithQuantities().entrySet().stream().filter(e -> e.getValue() > 0).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
  }

  public void consumeIngredients(Map<String, Integer> ingredients) {
    Set<Map.Entry<String, Integer>> ingredientsEntries = ingredients.entrySet();
    ingredientsEntries.stream().forEach(e -> ingredientsRepository.decrementIngredientQuantity(e.getKey(), e.getValue()));
  }

  public boolean verifyIngredients(Map<String, Integer> ingredients) {
    Set<Map.Entry<String, Integer>> ingredientsEntries = ingredients.entrySet();
    Optional<Map.Entry<String, Integer>> failedIngredient = ingredientsEntries.stream().filter(e -> ingredientsRepository.isIngredientNotAvailable(e.getKey(), e.getValue())).findFirst();
    return !failedIngredient.isPresent();
  }
}
