package com.ganchallenge.pizzaserver.services;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import javax.jms.Message;
import javax.jms.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import com.ganchallenge.pizzaserver.dto.OrderCriteria;

@Component
public class OrderQueueService {

  private static Logger log = LoggerFactory.getLogger(OrderQueueService.class);

  private JmsTemplate jmsTemplate;
  private Queue queue;
  @Value("${pizza-server.ovens}")
  private String[] ovensIdArr;

  public OrderQueueService(JmsTemplate jmsTemplate, Queue queue) {
    this.jmsTemplate = jmsTemplate;
    this.queue = queue;
  }

  public Message addOrderToQueue(OrderCriteria orderCrit) {
    final AtomicReference<Message> msg = new AtomicReference<>();
    jmsTemplate.convertAndSend(queue, orderCrit, m -> {
      msg.set(m);
      return m;
    });
    return msg.get();
  }

  public List<String> getOrdersInQueue() {
    return jmsTemplate.browse(queue, (session, browser) -> {
      List<String> orderList = new ArrayList<>();
      Enumeration<Message> enumeration = browser.getEnumeration();
      while (enumeration.hasMoreElements()) {
        Message msg = enumeration.nextElement();
        orderList.add(msg.getJMSMessageID());
      }
      return orderList;
    });
  }

  public boolean isOrderInQueue(String msgId) {
    int numOfOvens = ovensIdArr.length;
    return jmsTemplate.browse(queue, (session, browser) -> {
      Enumeration<Message> enumeration = browser.getEnumeration();
      for (int i = 0; enumeration.hasMoreElements(); i++) {
        Message msg = enumeration.nextElement();
        if (i < numOfOvens) {
          continue;
        }
        log.info("{} = {}", msg.getJMSMessageID(), msgId);
        if (msg.getJMSMessageID().equals(msgId)) {
          return true;
        }
      }
      return false;
    });
  }
}
