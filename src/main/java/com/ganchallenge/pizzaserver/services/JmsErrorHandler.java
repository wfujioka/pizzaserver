package com.ganchallenge.pizzaserver.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ErrorHandler;

@Service
public class JmsErrorHandler implements ErrorHandler {

  private static Logger log = LoggerFactory.getLogger(JmsErrorHandler.class);

  @Override
  public void handleError(Throwable t) {
    log.info(t.getMessage(), t);
  }

}
