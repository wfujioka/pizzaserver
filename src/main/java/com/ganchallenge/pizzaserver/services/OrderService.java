package com.ganchallenge.pizzaserver.services;

import javax.jms.JMSException;
import javax.jms.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ganchallenge.pizzaserver.dto.OrderCriteria;
import com.ganchallenge.pizzaserver.dto.OrderResponse;

@Service
public class OrderService {

  private static Logger log = LoggerFactory.getLogger(OrderService.class);

  private OrderQueueService queuingService;
  private IngredientsService ingredientsService;
  @Value("${pizza-server.baking-time}")
  private Integer bakingTime;


  public OrderService(OrderQueueService queuingService, IngredientsService ingredientsService) {
    this.queuingService = queuingService;
    this.ingredientsService = ingredientsService;
  }

  public OrderResponse prepareAndSendOrderForBaking(OrderCriteria orderCrit) {
    log.info("Order received");
    prepareForBaking(orderCrit);
    return createOrderResponse(queuingService.addOrderToQueue(orderCrit));
  }

  private OrderResponse createOrderResponse(Message msg) {
    OrderResponse orderResponse = new OrderResponse();
    orderResponse.setBakeTime(bakingTime);
    try {
      orderResponse.setMessageId(msg.getJMSMessageID());
    } catch (JMSException e) {
      throw new RuntimeException(e);
    }
    return orderResponse;
  }

  private void prepareForBaking(OrderCriteria orderCriteria) {
    ingredientsService.consumeIngredients(orderCriteria.getIngredients());
  }

}
