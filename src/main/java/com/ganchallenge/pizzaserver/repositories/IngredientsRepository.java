package com.ganchallenge.pizzaserver.repositories;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import com.ganchallenge.pizzaserver.util.CsvUtil;

@Repository
public class IngredientsRepository {

  private static Logger log = LoggerFactory.getLogger(IngredientsRepository.class);

  private Map<String, Integer> ingredientsCache;

  private CsvUtil csvUtil;

  @Value("${file.ingredient-csv.location}")
  private String ingredientsCsvLoc;

  public IngredientsRepository(CsvUtil csvUtil) {
    ingredientsCache = new ConcurrentHashMap<>();
    this.csvUtil = csvUtil;
  }

  @PostConstruct
  public void init() {
    loadIngredientsToCache();
  }

  public Map<String, Integer> getIngredientsWithQuantities() {
    return ingredientsCache;
  }

  public Set<String> getIngredients() {
    return ingredientsCache.keySet();
  }

  public void decrementIngredientQuantity(String ingredient, Integer quantity) {
    if (!ingredientsCache.containsKey(ingredient)) {
      throw new RuntimeException("ingredient " + ingredient + " does not exist");
    }

    Integer currentQuantity = ingredientsCache.get(ingredient);
    if (quantity > currentQuantity) {
      throw new RuntimeException(ingredient + " is no longer available");
    }
    Integer updatedQuantity = currentQuantity - quantity;
    ingredientsCache.put(ingredient, updatedQuantity);
    log.info("---- decrementing quantity for {} from {} to {}", ingredient, currentQuantity, updatedQuantity);
  }

  public boolean isIngredientNotAvailable(String ingredient, Integer quantity) {
    if (!ingredientsCache.containsKey(ingredient) || ingredientsCache.get(ingredient) < quantity) {
      return true;
    }
    return false;
  }

  private void loadIngredientsToCache() {
    List<String[]> lines = csvUtil.csvToList(ingredientsCsvLoc);
    for (String[] line : lines) {
      String ingredient = line[0];
      Integer quantity = Integer.parseInt(line[1]);
      ingredientsCache.put(ingredient, quantity);
    }
  }
}
