package com.ganchallenge.pizzaserver.config;

import java.util.Arrays;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import org.apache.activemq.command.ActiveMQQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;
import com.ganchallenge.pizzaserver.services.JmsErrorHandler;
import com.ganchallenge.pizzaserver.services.OvenListener;

@Configuration
@EnableJms
public class ActiveMqConfig implements JmsListenerConfigurer {

  private static Logger log = LoggerFactory.getLogger(ActiveMqConfig.class);

  private OvenListener ovenListener;
  private static final String QUEUE_ID = "pizzaserver.queue";
  @Value("${pizza-server.ovens}")
  private String[] ovensIdArr;

  public ActiveMqConfig(OvenListener ovenListener) {
    this.ovenListener = ovenListener;
  }

  @Bean
  public Queue createQueue() {
    return new ActiveMQQueue(QUEUE_ID);
  }

  @Bean
  public DefaultJmsListenerContainerFactory jmsFactory(ConnectionFactory connFactory, JmsErrorHandler errorHandler) {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
    factory.setConnectionFactory(connFactory);
    factory.setErrorHandler(errorHandler);

    return factory;
  }

  @Override
  public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
    log.info("Registering ovens...");
    Arrays.stream(ovensIdArr).forEach(i -> registerOven(i, registrar));
  }

  private void registerOven(String ovenId, JmsListenerEndpointRegistrar registrar) {
    registrar.registerEndpoint(createOven(ovenId));
    log.info("{} has been successfully registered", ovenId);
  }

  public SimpleJmsListenerEndpoint createOven(String ovenId) {
    SimpleJmsListenerEndpoint oven = new SimpleJmsListenerEndpoint();
    oven.setId(ovenId);
    oven.setDestination(QUEUE_ID);
    oven.setMessageListener(ovenListener);
    return oven;
  }
}
